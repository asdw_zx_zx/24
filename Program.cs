﻿using System;
using System.Collections.Generic;
 
using System.IO;
 

class Program
{
    static void Main(string[] args)
    {
        // 创建一个列表来存储所有展示的组合
        List<int[]> combinations = new List<int[]>();
        //创建一个列表来存储所有计算的组合
        List<int[]> combinations2 = new List<int[]>();
        //创建一个列表来存储所有计算成功的组合
        List<int[]> combinations3 = new List<int[]>();
        //创建一个列表来存储所有计算成功的组合
        List<int[]> combinations4 = new List<int[]>();
        // 用四重循环遍历所有可能的取法
        for (int i = 1; i <= 10; i++)
        {
            for (int j = 1; j <= 10; j++)
            {
                for (int k = 1; k <= 10; k++)
                {
                    for (int l = 1; l <= 10; l++)
                    {
                        // 创建一个数组来存储当前的组合
                        int[] combination = new int[4];
                        combination[0] = i;
                        combination[1] = j;
                        combination[2] = k;
                        combination[3] = l;

                        // 检查当前的组合是否已经存在于列表中，如果不存在，则添加到列表中
                        if (!Contains(combinations, combination))
                        {
                            combinations.Add(combination);
                        }
                        combinations2.Add(combination);
                    }
                }
            }
        }

        // 打印列表中的所有组合和解法

        foreach (int[] combination in combinations2)
        {
           
            string solution = Solve(combination);
            if (solution == null)
            {
       
            }
            else
            {
                combinations3.Add(combination);
            }
        }
        foreach (int[] combination in combinations)
        {
            int c = 0;
            foreach (int[] combinationA in combinations3) {
                if (eq(combination, combinationA)) {
                    c++;
                    if (combination[0] == 1 && combination[1] == 2 && combination[2] == 7 && combination[3] == 7)
                    {
                      
                        c++;
                        c--;
                    }
                } 
            }
            if (c > 0) combinations4.Add(combination);
                Console.WriteLine($"({combination[0]}, {combination[1]}, {combination[2]}, {combination[3]})"+"方法数："+c);

        }
        string[] a =new string[combinations4.Count];
        for (int i = 0; i < a.Length; i++) {
            a[i] = combinations4[i][0].ToString()+ combinations4[i][1].ToString()+ combinations4[i][2].ToString()+ combinations4[i][3].ToString();
        }
        createCsv(a);
        // 打印列表的长度，即组合的个数
        Console.WriteLine($"共有{combinations4.Count}种组合有结果");
    }

    // 定义一个辅助方法，用于判断一个列表是否包含一个数组
    static bool Contains(List<int[]> list, int[] array)
    {
        bool r = false;
        // 遍历列表中的每个数组
        foreach (int[] item in list)
        {
            r = eq(item, array);
            if (r) return r;
        }

        // 如果没有找到相等的数组，则返回false
        return r;
    }
    //比较两个数组 相等为true
   static bool eq(int[] item,int[] array) {
        // 如果两个数组的长度不相等，则跳过
        if (item.Length != array.Length)
        {
            return false;
        }

        // 创建两个新的数组，用于存储排序后的元素
        int[] sortedItem = new int[item.Length];
        int[] sortedArray = new int[array.Length];

        // 复制原始数组的元素到新的数组中
        Array.Copy(item, sortedItem, item.Length);
        Array.Copy(array, sortedArray, array.Length);

        // 对新的数组进行排序
        Array.Sort(sortedItem);
        Array.Sort(sortedArray);

        // 假设两个数组是相等的，除非发现不同的元素
        bool equal = true;

        // 遍历两个数组中的每个元素
        for (int i = 0; i < sortedItem.Length; i++)
        {
            // 如果发现不同的元素，则设置equal为false，并跳出循环
            if (sortedItem[i] != sortedArray[i])
            {
                equal = false;
                break;
            }
        }

        // 如果两个数组是相等的，则返回true
        if (equal)
        {
            return true;
        }
        return false;
    }

    // 定义一个方法，用于尝试找到一个解法
    static string Solve(int[] array)
    {
        // 定义一个数组，用于存储所有可能的运算符
        char[] operators = new char[] { '+', '-', '*', '/' };

        // 定义一个列表，用于存储所有可能的表达式
        List<string> expressions = new List<string>();

        // 用四重循环遍历所有可能的运算符组合
        for (int i = 0; i < operators.Length; i++)
        {
            for (int j = 0; j < operators.Length; j++)
            {
                for (int k = 0; k < operators.Length; k++)
                {
                    // 创建一个字符串，用于存储当前的表达式
                    string expression = "";

                    // 添加第一个数字和第一个运算符
                    expression += array[0];
                    expression += operators[i];

                    // 添加第二个数字和第二个运算符
                    expression += array[1];
                    expression += operators[j];

                    // 添加第三个数字和第三个运算符
                    expression += array[2];
                    expression += operators[k];

                    // 添加第四个数字
                    expression += array[3];

                    // 将当前的表达式添加到列表中
                    expressions.Add(expression);

                    // 创建一个新的字符串，用于存储带括号的表达式
                    string expressionWithBrackets = "";

                    // 在第一个和第二个数字之间添加括号
                    expressionWithBrackets += "(";
                    expressionWithBrackets += array[0];
                    expressionWithBrackets += operators[i];
                    expressionWithBrackets += array[1];
                    expressionWithBrackets += ")";
                    expressionWithBrackets += operators[j];

                    // 添加第三个数字和第三个运算符
                    expressionWithBrackets += array[2];
                    expressionWithBrackets += operators[k];

                    // 添加第四个数字
                    expressionWithBrackets += array[3];

                    // 将带括号的表达式添加到列表中
                    expressions.Add(expressionWithBrackets);

                    // 创建一个新的字符串，用于存储带括号的表达式
                    expressionWithBrackets = "";

                    // 在第二个和第三个数字之间添加括号
                    expressionWithBrackets += array[0];
                    expressionWithBrackets += operators[i];
                    expressionWithBrackets += "(";
                    expressionWithBrackets += array[1];
                    expressionWithBrackets += operators[j];
                    expressionWithBrackets += array[2];
                    expressionWithBrackets += ")";
                    // 添加第三个运算符和第四个数字
                    expressionWithBrackets += operators[k];
                    expressionWithBrackets += array[3];

                    // 将带括号的表达式添加到列表中
                    expressions.Add(expressionWithBrackets);

                    // 创建一个新的字符串，用于存储带括号的表达式
                    expressionWithBrackets = "";

                    // 在第三个和第四个数字之间添加括号
                    expressionWithBrackets += array[0];
                    expressionWithBrackets += operators[i];
                    expressionWithBrackets += array[1];
                    expressionWithBrackets += operators[j];
                    expressionWithBrackets += "(";
                    expressionWithBrackets += array[2];
                    expressionWithBrackets += operators[k];
                    expressionWithBrackets += array[3];
                    expressionWithBrackets += ")";

                    // 将带括号的表达式添加到列表中
                    expressions.Add(expressionWithBrackets);

                    // 创建一个新的字符串，用于存储带括号的表达式
                    expressionWithBrackets = "";

                    // 在第一个和第四个数字之间添加括号
                    expressionWithBrackets += "(";
                    expressionWithBrackets += array[0];
                    expressionWithBrackets += operators[i];
                    expressionWithBrackets += array[1];
                    expressionWithBrackets += operators[j];
                    expressionWithBrackets += array[2];
                    expressionWithBrackets += ")";
                    expressionWithBrackets += operators[k];

                    // 添加第四个数字
                    expressionWithBrackets += array[3];

                    // 将带括号的表达式添加到列表中
                    expressions.Add(expressionWithBrackets);
                }
            }
        }

        // 遍历列表中的每个表达式
        foreach (string expression in expressions)
        {
            // 尝试计算表达式的值
            try
            {
                double value = Evaluate(expression);

                // 如果值等于24，则返回表达式
                if (value == 24)
                {
                    if (array[0] == 7 && array[1] == 7 && array[2] == 1 && array[3] == 2)
                    {
                        int c = 0;
                        c++;
                        c--;
                    }
                    return expression + " = 24";
                }
            }
            catch (Exception e)
            {
                // 如果发生异常，则忽略该表达式
                continue;
            }
        }

        // 如果没有找到解法，则返回null
        return null;
    }

    // 定义一个方法，用于计算一个表达式的值
    static double Evaluate(string expression)
    {
        // 使用一个栈来存储操作数
        Stack<double> operands = new Stack<double>();

        // 使用一个栈来存储运算符
        Stack<char> operators = new Stack<char>();

        // 遍历表达式中的每个字符
        for (int i = 0; i < expression.Length; i++)
        {
            char c = expression[i];

            // 如果是空格，则跳过
            if (c == ' ')
            {
                continue;
            }

            // 如果是数字，则读取完整的数字，并将其转换为double类型，然后压入操作数栈中
            if (char.IsDigit(c))
            {
                string number = "";
                while (i < expression.Length && char.IsDigit(expression[i]))
                {
                    number += expression[i++];
                }
                i--;
                operands.Push(double.Parse(number));
            }

            // 如果是左括号，则压入运算符栈中
            else if (c == '(')
            {
                operators.Push(c);
            }

            // 如果是右括号，则弹出运算符栈中的运算符，并对操作数栈中的操作数进行计算，直到遇到左括号为止，然后将结果压入操作数栈中
            else if (c == ')')
            {
                while (operators.Peek() != '(')
                {
                    operands.Push(Calculate(operators.Pop(), operands.Pop(), operands.Pop()));
                }
                operators.Pop();
            }

            // 如果是加减乘除运算符，则先检查运算符栈是否为空，或者栈顶是否为左括号，如果是，则直接压入运算符栈中；否则，弹出运算符栈中的运算符，并对操作数栈中的操作数进行计算，然后将结果压入操作数栈中，再将当前的运算符压入运算符栈中
            else if (c == '+' || c == '-' || c == '*' || c == '/')
            {
                while (operators.Count > 0 && operators.Peek() != '(')
                {
                    operands.Push(Calculate(operators.Pop(), operands.Pop(), operands.Pop()));
                }
                operators.Push(c);
            }
        }

        // 当表达式遍历完毕后，如果运算符栈不为空，则继续弹出运算符栈中的运算符，并对操作数栈中的操作数进行计算，然后将结果压入操作数栈中，直到运算符栈为空为止
        while (operators.Count > 0)
        {
            operands.Push(Calculate(operators.Pop(), operands.Pop(), operands.Pop()));
        }

        // 此时，操作数栈中只剩下一个元素，即表达式的值，返回该值
        return operands.Pop();
    }

    // 定义一个方法，用于根据给定的运算符和操作数进行计算，并返回结果
    static double Calculate(char op, double b, double a)
    {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b;
            default:
                throw new Exception("Invalid operator");
        }
    }

 
    static void createCsv(string[] array)
    {

        // 将数组转换为CSV格式的字符串
        string csv = string.Join(",", array);

        // 指定文件名
        string fileName = "D:/array.csv";

        // 将字符串写入到文件中
        File.WriteAllText(fileName, csv);

        // 打印提示信息
        Console.WriteLine($"数组已保存到{fileName}文件中");
    }


}
